# -*- coding: utf-8 -*-


import flask
import requests

app = flask.Flask(__name__)

method_requests_mapping = {
    'GET': requests.get,
    'HEAD': requests.head,
    'POST': requests.post,
    'PUT': requests.put,
    'DELETE': requests.delete,
    'PATCH': requests.patch,
    'OPTIONS': requests.options,
}

@app.route('/<path:url>', methods=method_requests_mapping.keys())
def proxy(url):
    if flask.request.method == 'OPTIONS':
        return 'OK', 200, {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': 'authorization,content-type'}

    headers = {'Content-Type': 'application/json'}
    if 'Authorization' in flask.request.headers:
        headers.update({'Authorization': flask.request.headers['Authorization']})

    requests_function = method_requests_mapping[flask.request.method]
    request = requests_function(url, stream=True, params=flask.request.args,
        headers=headers, data=flask.request.data)
    response = flask.Response(flask.stream_with_context(request.iter_content()),
                              content_type=request.headers['Content-Type'] if 'Content-Type' in request.headers else 'application/json',
                              status=request.status_code)
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Expose-Headers'] = 'Request-Duration'
    response.headers['Request-Duration'] = request.elapsed.total_seconds()

    return response


@app.route('/ping')
def ping():
    return 'PONG', 200, {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': 'authorization,content-type'}


if __name__ == '__main__':
    app.debug = True
    app.run()
